import {Component, OnInit, ViewChild} from '@angular/core';
import {ModalController, NavParams} from '@ionic/angular';

import {Chart} from 'chart.js';

@Component({
    selector: 'app-modal-weather',
    templateUrl: './modal-weather.page.html',
    styleUrls: ['./modal-weather.page.scss'],
})
export class ModalWeatherPage implements OnInit {

    @ViewChild('tempCanvas') tempCanvas;
    @ViewChild('rainCanvas') rainCanvas;

    tempChart: any;
    rainChart: any;

    monthlyWeather: any;

    constructor(public modalCtrl: ModalController, public navParams:NavParams) {

    }

    ngOnInit() {
//176.63.111.143
        const actualTempForecast:any = [];
        const actualRainForecast:any = [];
        let rain;
        this.monthlyWeather = JSON.parse(this.navParams.get('data'));

        for(var i in this.monthlyWeather){

            switch(true){
                case ((<any>i) >= 0 && (<any>i) < 7):
                    actualTempForecast.push(Math.round(Math.random() * 2 + this.monthlyWeather[i].actualTemp) - 1);
                    rain = (Math.round(Math.random() * 2 + this.monthlyWeather[i].actualRain) - 1);
                    actualRainForecast.push( (rain < 0 ? 0 : rain) );
                    break;
                case ((<any>i) >= 7 && (<any>i) < 14):
                    actualTempForecast.push(Math.round(Math.random() * 4 + this.monthlyWeather[i].actualTemp) - 2);
                    rain = (Math.round(Math.random() * 4 + this.monthlyWeather[i].actualRain) - 2);
                    actualRainForecast.push( (rain < 0 ? 0 : rain) );
                    break;
                case ((<any>i) >= 14 && (<any>i) < 24):
                    actualTempForecast.push(Math.round(Math.random() * 6 + this.monthlyWeather[i].actualTemp) - 3);
                    rain = (Math.round(Math.random() * 6 + this.monthlyWeather[i].actualRain) - 3);
                    actualRainForecast.push( (rain < 0 ? 0 : rain) );
                    break;
                case ((<any>i) >= 24 && (<any>i) < 30):
                    actualTempForecast.push(Math.round(Math.random() * 12 + this.monthlyWeather[i].actualTemp) - 6);
                    rain = (Math.round(Math.random() * 12 + this.monthlyWeather[i].actualRain) - 6);
                    actualRainForecast.push( (rain < 0 ? 0 : rain) );
                    break;
            }

        }

        this.tempChart = new Chart(this.tempCanvas.nativeElement, {

            type: 'line',
            data: {
                labels: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"],
                datasets: [
                    {
                        label: "Temp",
                        fill: false,
                        lineTension: 0.1,
                        backgroundColor: "rgba(75,192,192,1)",
                        borderColor: "rgba(75,192,192,1)",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: "rgba(75,192,192,1)",
                        pointBackgroundColor: "#fff",
                        pointBorderWidth: 1,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "rgba(75,192,192,1)",
                        pointHoverBorderColor: "rgba(220,220,220,1)",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        data: actualTempForecast,
                        spanGaps: false,
                    },
                    {
                        label: "Rain",
                        fill: false,
                        lineTension: 0.1,
                        backgroundColor: "rgba(244,100,55,1)",
                        borderColor: "rgba(244,100,55,1)",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: "rgba(75,192,192,1)",
                        pointBackgroundColor: "#fff",
                        pointBorderWidth: 1,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "rgba(75,192,192,1)",
                        pointHoverBorderColor: "rgba(220,220,220,1)",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        data: actualRainForecast,
                        spanGaps: false,
                    }
                ]
            }

        });

    }

    save() {
        this.modalCtrl.dismiss();
    }

    ionViewDidLoad() {

    }

}
