import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalWeatherPage } from './modal-weather.page';

describe('ModalWeatherPage', () => {
  let component: ModalWeatherPage;
  let fixture: ComponentFixture<ModalWeatherPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalWeatherPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalWeatherPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
