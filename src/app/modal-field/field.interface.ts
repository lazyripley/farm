export interface Field {
  id: number;
  price: number;
  name: string;
  size: number;
}
