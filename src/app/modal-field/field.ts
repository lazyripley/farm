import { Field } from './field.interface';

export const FieldData: Field[] = [
  {
    id: 0,
    price: 1000,
    name: "fieldType1",
    size: 100
  },
  {
    id: 1,
    price: 2000,
    name: "fieldType2",
    size: 200
  },{
    id: 2,
    price: 3000,
    name: "fieldType3",
    size: 300
  },{
    id: 3,
    price: 4000,
    name: "fieldType4",
    size: 400
  },{
    id: 4,
    price: 5000,
    name: "fieldType5",
    size: 500
  },
];
