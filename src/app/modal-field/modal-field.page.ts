import { FieldData } from './field';
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Field } from './field.interface';

@Component({
  selector: 'app-modal-field',
  templateUrl: './modal-field.page.html',
  styleUrls: ['./modal-field.page.scss'],
})
export class ModalFieldPage {
  fieldData = FieldData;
  selectedFieldId: number;

  constructor(public modalCtrl: ModalController) { }

  save(){
    this.modalCtrl.dismiss({
      id: this.selectedFieldId
    });
  }

}
