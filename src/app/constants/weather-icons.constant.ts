export const WEATHER_ICONS: any = [
  {
      'id': 0,
      'status': 'sunny',
      'file': '/assets/weather/day_clear.png'
  },
  {
      'id': 1,
      'status': 'rainy',
      'file': '/assets/weather/day_rain.png'
  },
  {
      'id': 2,
      'status': 'cloudy',
      'file': '/assets/weather/day_partial_cloud.png'
  }
];