import { Component } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { Field } from '../modal-field/field.interface';
import { ModalSeedPage } from '../modal-seed/modal-seed.page';
import { ModalWeatherPage } from '../modal-weather/modal-weather.page';
import { ToastService } from '../services/toast.service';
import { WeatherIconService } from '../services/weather-icon.service';
import { WeatherIcons } from './../interfaces/weather-icons';
import { FieldData } from './../modal-field/field';
import { ModalFieldPage } from './../modal-field/modal-field.page';
import { WeatherGeneratorService } from './../services/weather-generator.service';

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
})
export class HomePage {

    /*
     date
     ------------------
     */
    actualDay = 1;
    actualMonth = 1;
    actualYear = 0;
    actualFullDate = 0;
    actualLastLoginDate = 0;
    actualDateTimeholder: any;

    lastLogin: any;
    actualLogin: any;

    different: number = 0;

    /*
     weather
     ------------------
     */
    actualWeather: any;
    actualTemp: any;
    actualRain: any;
    actualWeatherName: string = '';

    actualWeatherIcons: WeatherIcons = {
        day0: 0,
        day1: 0,
        day2: 0,
        day3: 0
    };

    monthlyWeather: any;

    /*
     game settings
     ------------------
     */
    speed = 300;

    /*
     own items
     ------------------
     */
    userSeed: any = [];
    userFields: Field[] = [];
    actualSeedID: number = 0;
    moneyMain: number = 10000;

    fieldTypes = FieldData;

    constructor(public alertController: AlertController,
                public modalController: ModalController,
                private weatherGeneratorService: WeatherGeneratorService,
                private weatherIconService: WeatherIconService,
                private toastService: ToastService) {
        var
            self = this;

        /*
         -------------------
         get the last login time
         ------------------------------
         */

        if (window.localStorage.getItem('farm/data/isSave') == 'true') {

            this.oldGame();

        } else {

            this.newGame();

        }

        this.userFields.push(FieldData[0]);

    }

    newGame() {
        var
            self = this;

        this.lastLogin = JSON.parse(window.localStorage.getItem('farm/data/actuallogtime'));
        this.actualLogin = JSON.parse(window.localStorage.getItem('farm/data/actuallogtime'));

        /*
         -------------------
         store the last login time
         ------------------------------
         */
        window.localStorage.setItem('farm/data/lastlogtime', window.localStorage.getItem('farm/data/actuallogtime'));

        this.init();

    }

    oldGame() {
        var
            self = this;

        this.lastLogin = JSON.parse(window.localStorage.getItem('farm/data/lastlogtime'));
        this.actualLogin = JSON.parse(window.localStorage.getItem('farm/data/actuallogtime'));

        /*
         -------------------
         store the last login time
         ------------------------------
         */
        window.localStorage.setItem('farm/data/lastlogtime', window.localStorage.getItem('farm/data/actuallogtime'));

        this.load();

    }

    init() {
        var
            self = this;

        /*
         -------------------
         calculate time different
         ------------------------------
         */
        // console.log(this.lastLogin, this.actualLogin)
        // let lastLoginDate: any = new Date(this.lastLogin.currentDateTime);
        // let actualLoginDate: any = new Date(this.actualLogin.currentDateTime);
        //
        // this.different = Math.round((Math.floor(Math.abs(actualLoginDate - lastLoginDate) / 60000) * ( 60 / (self.speed / 1000))) );
        //
        // console.log('------ TIMER DEBUG -------');
        // console.log('Last Login delayed date: ', Math.round((Math.floor(Math.abs(actualLoginDate - lastLoginDate) / 60000) )));
        // console.log('Minute / speed: ', ( 60 / (self.speed / 1000)) );
        // console.log('Last Login date: ', lastLoginDate);
        // console.log('Actual Login date: ', actualLoginDate);
        // console.log('Different: ', this.different);
        //
        // /*
        //  -------------------
        //  first branching
        //  ------------------------------
        //  */
        // console.log((window.localStorage.getItem('farm/save/isSave') !== null))
        // if (window.localStorage.getItem('farm/save/isSave') !== null) {
        //
        //     console.log('SAVE AG 1')
        //     this.load();
        //
        // } else {
        //
        //     console.log('SAVE AG 2')
        //     this.timer();
        //     this.weather();
        //
        // }

        this.timer();
        // this.weatherGenerate();

    }

    timer() {
        var
            self = this;

        const thirtyDayMonths = [3, 5, 8, 10];

        /*
         calculate actual date from elapsed time
         ------------------------------
         */
        if (this.actualDateTimeholder == undefined) {

            this.weatherGeneratorService.generateWeather(this.actualMonth);

        }

        this.actualDateTimeholder = setInterval(function () {

            switch (self.actualDay) {
                case 28:
                    if (self.actualMonth === 2) {
                        self.setNextMonth();
                    } else {
                        self.actualDay++;
                    }
                    break;
                case 30:
                    if (thirtyDayMonths.find(month => self.actualMonth === month)) {
                        self.setNextMonth();
                    } else {
                        self.actualDay++;
                    }
                    break;
                case 31:
                    self.setNextMonth();
                    break;
                default:
                    self.actualDay++;
                    break;
            }

            if (self.actualMonth > 12) {
                self.actualMonth = 1;
                self.actualYear++;
            }

            self.actualFullDate++;
            self.actualLastLoginDate++;

            self.weatherGeneratorService.generateWeather(self.actualMonth);
            self.weather();
            self.seed();
            // self.save();

        }, this.speed);

    }

    private setNextMonth() {
        var self = this;


        self.actualDay = 1;
        self.actualMonth++;
    }


    weather() {
        var
            self = this;


        this.actualWeather = [];
        this.monthlyWeather = this.weatherGeneratorService.getMonthlyWeather();

        this.actualWeather.push(this.monthlyWeather[this.actualDay]);
        this.actualTemp = this.monthlyWeather[this.actualDay].actualTemp;
        this.actualRain = this.monthlyWeather[this.actualDay].actualRain;
        this.actualWeatherName = this.monthlyWeather[this.actualDay].bigData.name;

        // TODO: handle next month weather at the end of the month
        this.actualWeatherIcons = this.weatherIconService.calculateWeatherIconByActualRain(this.monthlyWeather, this.actualDay);
    }

    seed() {
        var
            self = this,
            seed = JSON.parse(window.localStorage.getItem('farm/data/plants'));

        if (this.userSeed.length > 0)
            for (var i in this.userSeed) {

                // ----------------------
                // seed alive or died
                // ----------------------------

                if (!this.userSeed[i].die && this.userSeed[i].harvestTime >= 0) {

                    // ----------------------
                    // seed age
                    // ----------------------------
                    this.userSeed[i].ageFull = this.actualFullDate - this.userSeed[i].ageStart;
                    console.log(this.userSeed[i], (<any>this.userSeed[i]).plant)
                    this.userSeed[i].harvestTime = this.userSeed[i].plant.harvestTime - this.userSeed[i].ageFull;

                    // ----------------------
                    // seed condition
                    // ----------------------------
                    switch (true) {
                        // -------------------------------------------
                        // phase1
                        // -------------------------------------------
                        case (this.userSeed[i].ageFull >= 0 && this.userSeed[i].ageFull < this.userSeed[i].plant.phase1.day ):

                            // ----------------------
                            // temp
                            // ----------------------------
                            if ((this.userSeed[i].plant.phase1.temp - this.actualTemp) <= 4 && (this.userSeed[i].plant.phase1.temp - this.actualTemp) >= -4) {

                                this.userSeed[i].condition++;
                                this.userSeed[i].temp--;

                            } else {

                                this.userSeed[i].condition--;
                                this.userSeed[i].temp++;

                            }

                            // ----------------------
                            // water
                            // ----------------------------
                            if ((this.userSeed[i].plant.phase1.rain - this.actualRain) <= 3 && (this.userSeed[i].plant.phase1.rain - this.actualRain) >= -3) {

                                this.userSeed[i].condition++;
                                this.userSeed[i].rain--;

                            } else {

                                this.userSeed[i].condition--;
                                this.userSeed[i].rain++;

                            }

                            break;
                        // -------------------------------------------
                        // phase2
                        // -------------------------------------------
                        case (this.userSeed[i].ageFull >= this.userSeed[i].plant.phase1.day && this.userSeed[i].ageFull < this.userSeed[i].plant.phase2.day ):

                            // ----------------------
                            // temp
                            // ----------------------------
                            if ((this.userSeed[i].plant.phase2.temp - this.actualTemp) <= 3 && (this.userSeed[i].plant.phase2.temp - this.actualTemp) >= -3) {

                                this.userSeed[i].condition++;
                                this.userSeed[i].temp--;

                            } else {

                                this.userSeed[i].condition--;
                                this.userSeed[i].temp++;

                            }

                            // ----------------------
                            // water
                            // ----------------------------
                            if ((this.userSeed[i].plant.phase2.rain - this.actualRain) <= 3 && (this.userSeed[i].plant.phase2.rain - this.actualRain) >= -3) {

                                this.userSeed[i].condition++;
                                this.userSeed[i].rain--;

                            } else {

                                this.userSeed[i].condition--;
                                this.userSeed[i].rain++;

                            }


                            break;
                        // -------------------------------------------
                        // phase3
                        // -------------------------------------------
                        case (this.userSeed[i].ageFull >= this.userSeed[i].plant.phase2.day && this.userSeed[i].ageFull < this.userSeed[i].plant.phase3.day ):

                            // ----------------------
                            // temp
                            // ----------------------------
                            if ((this.userSeed[i].plant.phase3.temp - this.actualTemp) <= 8 && (this.userSeed[i].plant.phase3.temp - this.actualTemp) >= -8) {

                                this.userSeed[i].condition++;
                                this.userSeed[i].temp--;

                            } else {

                                this.userSeed[i].condition--;
                                this.userSeed[i].temp++;

                            }

                            // ----------------------
                            // water
                            // ----------------------------
                            if ((this.userSeed[i].plant.phase3.rain - this.actualRain) <= 3 && (this.userSeed[i].plant.phase3.rain - this.actualRain) >= -3) {

                                this.userSeed[i].condition++;
                                this.userSeed[i].rain--;

                            } else {

                                this.userSeed[i].condition--;
                                this.userSeed[i].rain++;

                            }

                            break;
                        // -------------------------------------------
                        // phase4
                        // -------------------------------------------
                        case (this.userSeed[i].ageFull >= this.userSeed[i].plant.phase3.day && this.userSeed[i].ageFull < this.userSeed[i].plant.phase4.day ):

                            // ----------------------
                            // temp
                            // ----------------------------
                            if ((this.userSeed[i].plant.phase4.temp - this.actualTemp) <= 10 && (this.userSeed[i].plant.phase4.temp - this.actualTemp) >= -10) {

                                this.userSeed[i].condition++;
                                this.userSeed[i].temp--;

                            } else {

                                this.userSeed[i].condition--;
                                this.userSeed[i].temp++;

                            }

                            // ----------------------
                            // water
                            // ----------------------------
                            if ((this.userSeed[i].plant.phase4.rain - this.actualRain) <= 3 && (this.userSeed[i].plant.phase4.rain - this.actualRain) >= -3) {

                                this.userSeed[i].condition++;
                                this.userSeed[i].rain--;

                            } else {

                                this.userSeed[i].condition--;
                                this.userSeed[i].rain++;

                            }

                            break;
                        // -------------------------------------------
                        // phase5
                        // -------------------------------------------
                        case (this.userSeed[i].ageFull >= this.userSeed[i].plant.phase4.day && this.userSeed[i].ageFull < this.userSeed[i].plant.phase5.day ):

                            // ----------------------
                            // temp
                            // ----------------------------
                            if ((this.userSeed[i].plant.phase5.temp - this.actualTemp) <= 10 && (this.userSeed[i].plant.phase5.temp - this.actualTemp) >= -10) {

                                this.userSeed[i].condition++;
                                this.userSeed[i].temp--;

                            } else {

                                this.userSeed[i].condition--;
                                this.userSeed[i].temp++;

                            }

                            // ----------------------
                            // water
                            // ----------------------------
                            if ((this.userSeed[i].plant.phase5.rain - this.actualRain) <= 3 && (this.userSeed[i].plant.phase5.rain - this.actualRain) >= -3) {

                                this.userSeed[i].condition++;
                                this.userSeed[i].rain--;

                            } else {

                                this.userSeed[i].condition--;
                                this.userSeed[i].rain++;

                            }

                            break;
                        default:
                            this.userSeed[i].condition = this.userSeed[i].condition - 2;
                            break;
                    }


                    switch (true) {
                        case (this.userSeed[i].condition < -20):

                            this.userSeed[i].die = true;

                            this.seedFreeze();
                            break;
                    }

                }

            }

    }

    private seedFreeze() {
        console.log('seed freezed');

    }

    private seedBurn() {

    }

    private seedHarvest() {

    }

    private seedNeedWater() {

    }

    private seedNeedHot() {

    }

    save() {
        var
            self = this;

        /*
         have a save
         ------------------
         */
        window.localStorage.setItem('farm/save/isSave', 'true');

        /*
         date
         ------------------
         */
        window.localStorage.setItem('farm/save/actualDay', this.actualDay.toString());
        window.localStorage.setItem('farm/save/actualMonth', this.actualMonth.toString());
        window.localStorage.setItem('farm/save/actualYear', this.actualYear.toString());
        window.localStorage.setItem('farm/save/actualFullDate', this.actualFullDate.toString());
        window.localStorage.setItem('farm/save/actualLastLoginDate', this.actualLastLoginDate.toString());

        /*
         seed
         ------------------
         */
        console.log(this.userSeed, this.actualFullDate);
        window.localStorage.setItem('farm/save/userSeed', JSON.stringify(this.userSeed));

    }

    load() {
        var
            self = this;

        /*
         reset
         -------------------
         */
        this.reset();

        /*
         date
         ------------------
         */
        clearInterval(this.actualDateTimeholder);
        this.actualDay = parseInt(window.localStorage.getItem('farm/save/actualDay'));
        this.actualMonth = parseInt(window.localStorage.getItem('farm/save/actualMonth'));
        this.actualYear = parseInt(window.localStorage.getItem('farm/save/actualYear'));
        this.actualFullDate = parseInt(window.localStorage.getItem('farm/save/actualFullDate'));

        /*
         add elapsed time
         ------------------
         */


        /*
         seed
         ------------------
         */
        console.log(JSON.parse(window.localStorage.getItem('farm/save/userSeed')))
        if (window.localStorage.getItem('farm/save/userSeed') !== '') {
            this.userSeed = JSON.parse(window.localStorage.getItem('farm/save/userSeed'));
        }

        /*
         call entry functions
         ------------------
         */
        this.timer();
        // this.weather();

    }

    async buy() {
        var
            self = this,
            seed = JSON.parse(window.localStorage.getItem('farm/data/plants'));

        if (this.fieldSize > 0) {
            const modal = await this.modalController.create({
                component: ModalSeedPage,
                componentProps: {
                    availableFields: this.userFields
                }
            });

            modal.onDidDismiss()
                .then((data) => {
                    if (data) {
                        const { plant, field } = data.data;
                        const newSeed = {
                            'id': self.actualSeedID,
                            'plant': seed[plant],
                            'type': seed[plant].type,
                            'ageYear': self.actualYear,
                            'ageMonth': self.actualMonth,
                            'ageDay': self.actualDay,
                            'ageFull': 0,
                            'ageStart': self.actualFullDate,
                            'rain': 0,
                            'temp': 0,
                            'crop': 0,
                            'phase': 1,
                            'die': false,
                            'harvestTime': 0,
                            'condition': seed[plant].idealFieldTypes.includes(parseInt(field)) ? 5 : -5,
                            'selectedField': this.fieldTypes[field]
                        };
                        this.userSeed.push(newSeed);
                        this.userFields.splice(this.userFields.findIndex(userField => userField.id === parseInt(field)), 1);
                    }
                });

            return await modal.present();
        } else {
            this.toastService.present("You need to buy fields to plant seeds.")
        }

    }

    removeSeed(plantId, fieldId) {
        this.userSeed.splice(this.userSeed.findIndex(e => e.id === parseInt(plantId)), 1);
        this.userFields.push(this.fieldTypes.find(userField => userField.id === parseInt(fieldId)));
    }

    async buyField() {

        const modal = await this.modalController.create({
            component: ModalFieldPage,
        });
        console.log(modal);
        modal.onDidDismiss()
            .then((modalData) => {
                let selectedField: Field;

                if (modalData.data) {
                    selectedField = this.fieldTypes.find((field: Field) => field.id === parseInt(modalData.data.id));

                    if (selectedField && this.moneyMain >= selectedField.price) {
                        this.moneyMain -= selectedField.price;
                        this.userFields.push(selectedField);
                        this.toastService.present('You bought a field.');

                    } else {
                        this.toastService.present('You do not have enough money to buy this field.');
                    }
                }
            });

        return await modal.present();

    }

    async weatherModal() {
        var
            self = this;

        const modal = await this.modalController.create({
            component: ModalWeatherPage,
            componentProps: {
                data: JSON.stringify(this.monthlyWeather)
            }
        });

        return await modal.present();

    }

    reset() {
        this.userSeed = [];
    }

    get fieldSize() {
        return this.userFields.reduce((sum, field) => sum + field.size, 0);
    }

}
