import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import {ModalFieldPage} from './modal-field/modal-field.page';
import {ModalSeedPage} from './modal-seed/modal-seed.page';
import {ModalWeatherPage} from './modal-weather/modal-weather.page';

@NgModule({
  declarations: [AppComponent, ModalSeedPage, ModalWeatherPage, ModalFieldPage],
  entryComponents: [ModalSeedPage, ModalWeatherPage, ModalFieldPage],
  imports: [BrowserModule, IonicModule.forRoot( ), HttpClientModule, AppRoutingModule, FormsModule],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
