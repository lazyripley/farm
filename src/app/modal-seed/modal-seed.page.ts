import { Component, OnInit } from '@angular/core';
import { FormsModule  } from '@angular/forms';
import { ModalController, NavParams } from '@ionic/angular';
import { Field } from '../modal-field/field.interface';

@Component({
  selector: 'app-modal-seed',
  templateUrl: './modal-seed.page.html',
  styleUrls: ['./modal-seed.page.scss'],
})
export class ModalSeedPage implements OnInit {

  plantsData: any = [];
  selectedPlant: any;
  availableFields: Field[];
  selectedField: number;

  constructor(public modalCtrl: ModalController, navParams: NavParams) {

    /*
     -------------------
     weather
     ------------------------------
     */
    this.plantsData = JSON.parse(window.localStorage.getItem('farm/data/plants'));
    this.availableFields = navParams.get('availableFields');
  }

  ngOnInit() {
  }

  save(){
    this.modalCtrl.dismiss({
      plant: this.selectedPlant,
      field: this.selectedField
    });
  }

}
