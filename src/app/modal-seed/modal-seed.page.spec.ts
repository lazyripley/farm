import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalSeedPage } from './modal-seed.page';

describe('ModalSeedPage', () => {
  let component: ModalSeedPage;
  let fixture: ComponentFixture<ModalSeedPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalSeedPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalSeedPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
