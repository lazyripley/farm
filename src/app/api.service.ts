import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  baseURL: string = ''

  data: any;
  headers: any;
  headersFormData: any;

  constructor(private httpClient: HttpClient) {

    /*
     -----------------------------------
     HEADER FORM DATA
     -----------------------------------
     */
    this.headersFormData = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded'
      })
    };
    /*
     -----------------------------------
     HEADER
     -----------------------------------
     */
    this.headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
  }

  /*
   -----------------------------------
   GET OBJECT SIZE
   -----------------------------------
   */
  getObjectSize(obj) {
    var size = 0, key;
    for (key in obj) {
      if (obj.hasOwnProperty(key)) size++;
    }
    return size;
  }

  /*
   -----------------------------------
   ERROR MESSAGES
   -----------------------------------
   */
  errorMessages(errorData, success) {
    var
        key,
        errorArray = [],
        self = this;

    /*
     reset error messages
     ------------------------
     */
    var elements = document.getElementsByClassName('error--message');
    while(elements.length > 0){
      elements[0].parentNode.removeChild(elements[0]);
    }

    /*
     false isValid
     ------------------------
     */
    if (!errorData.isValid) {

      if (this.getObjectSize(errorData.messages) > 0) {
        for (key in errorData.messages) {
          console.log(key)
          if (errorData.messages.hasOwnProperty(key)) {

            if (Object.values(errorData.messages[key]).length < 2) {

              console.log(Object.values(errorData.messages[key]).length, Object.values(errorData.messages[key]))

              errorArray.push(key + ' ' + Object.values(errorData.messages[key])[0])
            } else {

              console.log(Object.values(errorData.messages[key]).length)

              errorArray.push(key + ' ' + Object.values(errorData.messages[key])[0])
            }
          }

          /*
           -------------------------
           add error elems
           ---------------------------------
           */
          var newNode = document.createElement('span');
          newNode.innerHTML = '<span class="error--message ml-2">' + Object.values(errorData.messages[key])[0] + '</span>';
          document.querySelector("label[for=" + key + "]").after(newNode);

        }
// console.log( document.getElementById('form-message'))
        /*
         -------------------------------------
         alert popup
         *--------------------------------------
         */
        // document.getElementById('form-message').innerHTML = '';
        // document.getElementById('form-message').innerHTML =  '<span>' + errorArray.join().replace(/,/g, '<br><div></div>') + '</span>';

        // alert(errorArray.join().replace(/,/g, '') );
        // let alert = this.alertController.create({
        //   title: 'Error',
        //   subTitle: '<span>' + errorArray.join().replace(/,/g, '<br><div></div>') + '</span>',
        //   buttons: ['OK']
        // });
        // alert.present();
      }
    }
    /*
     other server error (invalid token, etc)
     -------------------------
     */
    if (errorData.statusText == "Forbidden") {

      alert('forbidden')
      // let alert = this.alertController.create({
      //   title: 'Error',
      //   subTitle: '<span>' + errorData.statusText + ' - Please login again</span>',
      //   buttons: [
      //     {
      //       text: 'OK',
      //       handler: () => {
      //
      //       }
      //     }]
      // });
      // alert.present();

    }
    /*
     true isValid
     ------------------------
     */
    if (errorData.isValid) {
      success(errorData);
    }
  }

  /*
   -----------------------------------
   Email messages / POST message
   -----------------------------------
   */
  // postEmailMessage (hero: Hero): Observable<Hero> {
  //   return this.httpClient.post<Hero>(this.baseURL + '/api/v1/companies/13/email/letters?envelope=true', hero, this.headersFormData)
  //       .pipe(
  //           catchError(this.handleError('addHero', hero))
  //       );
  // }
  getActualTime(payload, success) {
    var
        self = this;

    return new Promise(resolve => {
      this.httpClient.get('http://worldclockapi.com/api/json/utc/now')
          .subscribe( (data: any) => {
            this.data = data;

            resolve(this.data);
            self.errorMessages(data, success);
          }, function (data) {
            console.log('error', data);
            self.errorMessages(data, success);
          });
    });

  }
  getCountryData(payload, success) {
    var
        self = this;

    return new Promise(resolve => {
      this.httpClient.get(this.baseURL + 'assets/data/country.json', {responseType: 'text'})
          .subscribe( (data: any) => {
            this.data = data;

            resolve(this.data);
            self.errorMessages(data, success);
          }, function (data) {
            console.log('error', data);
            self.errorMessages(data, success);
          });
    });

  }
  getWeatherData(payload, success) {
    var
        self = this;

    return new Promise(resolve => {
      this.httpClient.get(this.baseURL + 'assets/data/weather.json', {responseType: 'text'})
          .subscribe( (data: any) => {
            this.data = data;

            resolve(this.data);
            self.errorMessages(data, success);
          }, function (data) {
            console.log('error', data);
            self.errorMessages(data, success);
          });
    });

  }
  getPlantsData(payload, success) {
    var
        self = this;

    return new Promise(resolve => {
      this.httpClient.get(this.baseURL + 'assets/data/plants.json', {responseType: 'text'})
          .subscribe( (data: any) => {
            this.data = data;

            resolve(this.data);
            self.errorMessages(data, success);
          }, function (data) {
            console.log('error', data);
            self.errorMessages(data, success);
          });
    });

  }
}
