import { Pipe, PipeTransform } from '@angular/core';
import { WEATHER_ICONS } from '../constants/weather-icons.constant';

@Pipe({
  name: 'weatherIcon'
})
export class WeatherIconPipe implements PipeTransform {

  transform(id: string): string {
    return WEATHER_ICONS[parseInt(id)].file;
  }

}
