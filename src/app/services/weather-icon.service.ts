import { WeatherIcons } from "./../interfaces/weather-icons";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class WeatherIconService {
  calculateWeatherIconByActualRain(
    monthlyWeather: any,
    actualDay: number
  ): WeatherIcons {
    return {
      day0: this.getIconNumber(monthlyWeather[actualDay + 0].actualRain),
      day1:
        actualDay + 1 <= 31
          ? this.getIconNumber(monthlyWeather[actualDay + 1].actualRain)
          : 0,
      day2:
        actualDay + 2 <= 31
          ? this.getIconNumber(monthlyWeather[actualDay + 2].actualRain)
          : 0,
      day3:
        actualDay + 3 <= 31
          ? this.getIconNumber(monthlyWeather[actualDay + 3].actualRain)
          : 0,
    };
  }

  private getIconNumber(actualRain: number): number {
    let result = 1;

    if (actualRain === 0) {
      result = 0;
    } else if (actualRain > 0 && actualRain < 3) {
      result = 2;
    }

    return result;
  }
}
