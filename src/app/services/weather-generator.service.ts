import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class WeatherGeneratorService {
  private monthlyWeather = [];
  private weatherData = JSON.parse(window.localStorage.getItem('farm/data/weather'));
  private monthlyDatabaseWeatherData = JSON.parse(window.localStorage.getItem('farm/data/country'));

  generateWeather(actualMonth: number): void {
    this.monthlyWeather = [];
    let min,
        max,
        minRain,
        maxRain,
        randomWeatherData,
        randomNumberForBigValues,
        lastRandomNumberForBigValues = 0,
        randomRainForBigValues,
        lastRandomRainForBigValues = 0;

    for (let i = 0; i < 32; i++) {
        min = -1;
        max = 1;
        minRain = -10;
        maxRain = 3;
        randomWeatherData = Math.round(Math.random() * 9);
        randomNumberForBigValues = lastRandomNumberForBigValues + (Math.round(Math.random() * (max - min)) + min);
        randomRainForBigValues = lastRandomRainForBigValues + (Math.round(Math.random() * (maxRain - minRain)) + minRain);

        const monthIndex = actualMonth - 1;
        this.monthlyWeather.push({
          'bigData': this.weatherData[randomWeatherData],
          'actualTemp': this.monthlyDatabaseWeatherData[0].weather[monthIndex].temp +
            (this.weatherData[randomWeatherData].temp + randomNumberForBigValues),
          'actualRain': Math.trunc(this.monthlyDatabaseWeatherData[0].weather[monthIndex].rain +
            (this.weatherData[randomWeatherData].rain + randomRainForBigValues) < 0 ? 0 :
            (this.monthlyDatabaseWeatherData[0].weather[monthIndex].rain +
              (this.weatherData[randomWeatherData].rain + randomRainForBigValues)) / 10),
        });

        lastRandomNumberForBigValues = randomNumberForBigValues;
        lastRandomRainForBigValues = randomRainForBigValues;
    }
  }

  getMonthlyWeather() {
    return this.monthlyWeather;
  }
}
