import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

import {ApiService} from '../api.service';
import {HomePage} from "../home/home.page";

@Component({
    selector: 'app-init',
    templateUrl: './init.page.html',
    styleUrls: ['./init.page.scss'],
})
export class InitPage implements OnInit {

    startIsDisabled: boolean = true;

    constructor(private api: ApiService, private router: Router) {
        var
            self = this;

        /*
         -------------------
         store some datas
         -----------------------------
         */
        this.api.getCountryData('', function () {

        })
            .then(dataCountry => {

                /*
                 ---------------------
                 store country data
                 -----------------------------
                 */
                window.localStorage.setItem('farm/data/country', (<any>dataCountry));

                self.api.getWeatherData('', function () {

                })
                    .then(dataWeather => {

                        /*
                         ---------------------
                         store weather data
                         -----------------------------
                         */
                        window.localStorage.setItem('farm/data/weather', (<any>dataWeather));

                        self.api.getPlantsData('', function () {

                        })
                            .then(dataPlants => {

                                /*
                                 ---------------------
                                 store plants data
                                 -----------------------------
                                 */
                                window.localStorage.setItem('farm/data/plants', (<any>dataPlants));

                                /*
                                 ---------------------
                                 actual date
                                 -----------------------------
                                 */
                                self.api.getActualTime('', function () {

                                })
                                    .then(dataTime => {

                                        /*
                                         ---------------------
                                         store plants data
                                         -----------------------------
                                         */
                                        window.localStorage.setItem('farm/data/actuallogtime', JSON.stringify((<any>dataTime)));

                                        self.startIsDisabled = false;

                                    })

                            })

                    })

            })

    }

    ngOnInit() {


    }

    start() {
        var
            self = this;

        /*
         ---------------------
         start
         -----------------------------
         */
        self.router.navigateByUrl('/home');
    }

}
